import {createWebHistory, createRouter} from "vue-router";
import Home from "../views/perpus/Index.vue";
import Edit from "../views/perpus/Edit.vue";
import Create from "../views/perpus/Create.vue";
import Buku from "../views/perpus/IndexBuku.vue";
import BukuEdit from "../views/perpus/EditBuku.vue";
import BukuCreate from "../views/perpus/CreateBuku.vue";
const routes = [
    {
        path:'/create',
        name: 'perpus.create',
        component: Create,
    },
    {
        path:'/edit/:id',
        name: 'perpus.edit',
        component: Edit,
    },
    {
        path:'/',
        name: 'perpus.index',
        component: Home,
    },
    {
        path:'/buku',
        name: 'perpus.indexBuku',
        component: Buku,
    },
    {
        path:'/editBuku/:id',
        name: 'perpus.editBuku',
        component: BukuEdit,
    },
    {
        path:'/createBuku',
        name: 'perpus.createBuku',
        component: BukuCreate,
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;